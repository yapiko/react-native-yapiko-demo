# React native yapiko demo app

This app is meant to be developed just to show the potential use of React Native.


## Installation
1. git clone git@bitbucket.org:yapiko/react-native-yapiko-demo.git
2. npm install
4. react-native run-ios

## Presentation of React Native
Find a powerpoint of React Native in the Download section: https://bitbucket.org/yapiko/react-native-yapiko-demo/downloads/ 

## Build with
- [react-native-ui-kitten](https://github.com/akveo/react-native-ui-kitten)
- [react-native-vector-icons](https://github.com/oblador/react-native-vector-icons)
- [react-native-drawer](https://github.com/root-two/react-native-drawer)
